# Intro

This repository contains two main things:

1. Companion notes for "Learn Ruby the Hard Way" that will help bridge the gap
   between the instruction in the book and how things are applied in the real
   world of `payrails` The book notes use the same numbering scheme as the
   exercises in the book, so the notes for "Exercise 0" in the book are in the
   file `reading-notes/0.md`
2. A project for you to complete, called `paytools`, that will walk you through
   building your own tools for enhancing `payrails`. The `paytools` project
   exercises are inside the `paytools/` folder, and are numbered in the order
   in which you should complete them.

## Submitting work

A great way to get feedback as you go along through this book is to create a
repository of program code and output, and submit them for review when you're
done.

I highly recommend doing all the exercises, as they are presented in the book,
if you want maximum benefit. Since you've alreay done some ruby programming some
of the exercises will be trivially easy for you, maybe even boring. This is
okay. The goal of going through this, methodically, is so that you have a
systematically placed foundation under you.

## Is this really relevant?

I'm sure a fair amount of this book is going to seem trivial, even possibly
irrelevant. As you progress in your programming skills you may look back at the
exercises in this book and wonder why I had you start from such humble
beginnings. The reason isn't because I don't think you're capable of great
things, it's because I don't want you to skip the details that most programmers
take for granted and often assume everyone already knows. Nearly everything in
this book is used directly or knowledge of the concepts is assumed when working
in `payrails`, so the goal is to give you that foundation that most of the
dedicated `payrails` developers assume each other have, and to bring you into
the fold.

After you complete this book you'll have a strong foundation
for these core concepts:

* Data types
* Input and output
* Common data structures in ruby (hashes and arrays being some of the most
  powerful)
* A very simple intro to web programming using Sinatra

After you're done with the book we'll start to branch out into much harder
things:

* Rails, MVC, and the web
* Algorithms
* Ruby's `Enumerable` module, and doing complex data transformations by
  breaking them up into smaller steps that can be chained together
* A deeper dive into test-driven development
* Object-orinted design
* Large project code organization
* Working with the messy work of business rules
* Refactoring and dealing with change
* Metaprogramming
* Domain-specific languages (DSLs) - hint: RSpec is a DSL

Happy coding!
