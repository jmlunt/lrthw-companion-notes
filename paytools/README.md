# paytools

`paytools` is a project that will have you apply your ruby skills to the real
world by building custom enhancements for `payrails` right into the application
itself.

As you gain skills from "Learn Ruby the Hard Way" come back to this project so
that you can apply your learning directly to `payrails` enhancements.
